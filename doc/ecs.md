# Entity/Component/System

## Intro

This is a thought dump on ECSes, a.k.a. Component-Entity-Processor
architecture, and their applicapability to games with massive
detailed worlds (read: simulations).

In a way, it can be thought of as a design doc prior to introducing
a specific ECS library (`esper`), and an attempt to lay bare the
quirks and short-comings beforehand.

It can also be seen as procrastination by trying to solve an issue that
doesn't exist yet.


## ECS performance depending on world size

ECSes don't perform so well when there's a lot of entities. Unless,
of course, the database in the back is funky, and the interface
in the front digs it.

In particular, `esper`'s benchmarks show me:

N-components lookup | 5'000-entity world | 50'000-entity world
:-----------------: | :---------------: | :-----------------:
1                   | 1.25 ms           | 17 ms
2                   | 2.25 ms           | 30 ms
3                   | 2.50 ms           | 31 ms

For 30 FPS, a frame must not take longer than 33.(3) ms. Choppiness
is as much an issue for text display as it is for graphical.

If there are 50'000 entities, having just two 1-component processors
will eat up all the allotted time - just for lookups, no logic or
entity manipulation!

If there are 5'000 entities, there could be up to 26.4 1-component
processors, 14.(6) 2-component, or 13.2 3-component ones (but, again,
that's just lookups). This could be acceptable, especially taking
parallelisation into account.


## World size comparison

Say, there are these:

size   | Lvl cnt | Avg lvl size | Tiles      | /w objs (*1.5) | objs 
:----- | :------ | :----------: | ---------: | -------------: | ---------:
tiny   | 3       |    10x10     |        300 |            450 |        150
small  | 7       |    50x50     |     17'500 |         26'250 |      8'750
medium | 13      |   100x100    |    130'000 |        195'000 |     65'000
large  | 29      |   500x500    |  7'250'000 |     10'875'000 |  3'625'000
huge   | 57      | 1'000x1'000  | 57'000'000 |     87'500'000 | 28'000'000

(The number of objects being half that of tiles is an estimate "on the
safe side". After all, space ships are likely to be crammed, and
corridors being only as big as what's supposed to fit in them.)

Apparently, nothing but _tiny_ worlds could use "direct lookups" if every
tile (e.g. floor+ceiling) was an entity and processors weren't smart.

However, a tile entity might just have a Position(x,y,z) component,
and perhaps a Surface(6-cardinals) for floor+ceiling, which no
processor would be using as keys for look-ups...

(Need to access by position exists, but that could be handled by a
method in a Level object (entity? component? non-ECS?). In a way,
that would be a use-specific cache.)

So, maybe even _small_ worlds can use "direct lookups", if they're smart.
Yet, _medium_ seems like the smallest acceptable ATM, and at least _large_
desirable for "replay value" reasons (space for extras or alternative
progression, when set in the same world).

Otherwise, fragmentation by reality bubble is required very early on,
where their size makes them annoyingly apparent.


## Memory footprint and reality bubbles

If an average object was 4 KiB, then _medium_ would have a memory
footprint of ~254 MiB (for objects alone!). Theoretically manageable
on modern computers.

Savefiles would be humongous, though, and could cause an annoyng
delay during (de)serialisation. 

However, object count is already so high that "direct lookups" are
likely no longer feasible. Some sort of caching and priority assigning
are now required - possibly solvable with actors passing messages.

_large_ would take > 14 GiB in memory, at which point it's not
practical to store, leave alone process. Introducing reality
bubbles (plural!) may be useful, probably with variable size. All
player-controlled characters (e.g. a "main" one and a remotely
controlled robot) would have to be in a bubble of their own. Non-
player characters, and maybe even creatures, could take turns in
the background.

It is possible that introducing reality bubbles would be useful even
for smaller, _medium_ worlds, to reduce the memory footprint.

If reality bubbles are implemented as separate "worlds" (entity
databases with processors attached), then they must not overlap, so
"double processing" is avoided.


## Messaging

How could it be achieved?

One way is using a generic `State`/`Message`/`Changed` component, adding
it to an entity that's been "dirtied". Processors could specifically
look up these, and act on them if needed, possibly removing the
component if it's been "cleaned up".

This would probably require a separate component type for every possible
processor.

It's also hackish and might not cover some edge cases, like a red-hot
piece of steel "colliding" with water while flying out into vacuum
through the airlock. Meaning that instead of two `Message`-like components
`Evaporating` and `Cooling` affecting the objects' `Temperatures`, there
would be `{Evap,Cool}-Due-To-X`, or some other way to reconcile this
three-object system.

It gets very hairy with vacuum, too, which shouldn't be an entity or any
object for that matter, otherwise the world's corners would always be
"filled" by it... And it literally does nothing!

(It seems that components are well-suited for storing "state data", but
maybe not so much for "transition data" - in state machine terms. What
ECS tries to emulate is the "real world", where universal laws apply in
parallel to homogenous particles. This falls face-down when performed
sequentially on a CPU.)

A different approach is adding a "message queue" to processors
themselves. With `esper` - by expanding `Processor` class with, say,
a `message()` method that pushes to a `messages` queue. When their
`process()` is called, instead of doing a "global lookup" in the
database, processors would iterate through their queue, and act on:

* component instances - not sure if needed (or possible without `id`s)
* entity instances - easy, just need `id`
* component classes - the only choice without messaging
* NOT entity classes - there is no such thing!

Basically, it becomes possible to single out entities that need changing.

Such a system can "run itself" indefinitely if no user input is required.
In fact, UI acts as an "injector" of outside messages into the simulation.
The distinction between turn-based and real-time is blurred - the former
just pauses execution when acting is possible.


## Message propagation "waterfall"

Say, user input is detected by `CursesInputManager`. Context is "main
window", displayed as "CAMERA" ATM.

Say, an input handler has been registered with this context, probably in
`callbacks` via `call_me()`. It's a processor called `PlayerActor` to
whose `messages` the command will be written, together with the origin.
If timestamps are not needed, it's just:

``` python
{'src': CursesInputManager, 'msg': [keycode, keycode, ...]}
```

De/serialising on receive/send is expensive! Try to avoid if possible.
Passing a key-value map can be safe enough at first, but what if the
format changes?..

Anyway, all `PlayerActor` would end up doing is translation from "user
input" to entity manipulation, namely adding an `Acceleration`
component to the entity if it's not present, or perhaps changing it
if it is.

`PlayerActor` has a `callbacks` of its own. Say, it decoded its
message as "move left", so it sends another message to... `Physics`?

`Physics` now knows there's a new object to track. It can calculate
velocity deltas and touch `Velocity` components, then remove
`Acceleration` if it's not continuous. It can then normally process
all entities with `Velocity`, changing their `Position` or adding
a `Collision` component.

Note that the above can easily be done without `PlayerActor`
messaging `Physics`, but by `Physics` processing `Acceleration` first.
However, this has the cost of lookups, as described above.

For entities under continuous acceleration, such as vehicles or
spacecraft, `Physics` could add a message to itself. This would
end up looking like a "tracked objects cache", and theoretically
might be needed to reduce churn if there's a lot of objects to track.
However, this can be seen as a `Physics`' implementation detail.

The `Collider` processor - essentially an offshoot of `Physics`,
from which it should receive messages detailing N-entity systems, -
would calculate the _results_ of a collision, probably changing
entities, creating new ones, etc..

A thrown wrench could break a display. A creature could push
against a wall, or smash into it if it's running.


## Reconciling conflicting approaches

### Homogenous processing by canonical ECS

The very purpose of ECSes is not requiring out-of-order calls to
specific world entities, as commonly done with "object-oriented
hierarchy" approaches.

Components are processed in bulk, with disregard for origin (external
input or normal simulation) or other properties that could give
"special snowflake" status.

"Caching" is not done explicitly. It can be done transparently at the
database level at best, but the programmatic interface is always the
same: a `SELECT`-like database lookup. Optionally, a `JOIN`-like one,
too.

The programmer should not be required to know that when adding a
specific component, a processor must be notified.


### Non-homogenous processing with messaging

Messaging, as described previously, allows preemptive notification, as
in "set has changed". Yet, what it does is assign priority, by virtue of
specifying _what_ is different.

(Removing this detail would achieve faster messaging - one less thing
to pass. The processor would still know whether it has to run at all.
Still, if it has to, then it's best to know why so and when it's OK to
stop.)

So, a processor "knows" a subset of entities it should be processing,
and can act on them before (or while!) fetching the others.


### Priority via sliced database lookups

What could be done instead of "messaging" is adding components to a
database "slice", in effect preemptively partitioning it. A processor
could then request "next bucket, please" from the database, instead of
"give me everything you've got!".

(This approach is [described here by Kevin Granade][kevinddabucket].)

The order in which slices are returned by the database effectively
designates their priority.

By reordering slices, priority can be manipulated. Ordering schemes may
be a "flat" sequence or more complex. Imagine a system that "knows" to
return the slices from a player character's reality bubble first, then
from other reality bubbles, possibly checking in between whether it
still fits in the allotted time frame. This would eschew the need for
multiple runtime entity databases (aka "worlds") and separate sets of
processors for each.


## Reiteration

Processors should be independent! If that seems impossible, then
it is probable that the processors in question are actually
"sub-processors", like in the case of `Collider` and `Physics`.

Imagine two processors `P1` and `P2` that need to access components of
type `C1` in a given time frame. Say, there are many entities with this
component, so it's separated into slices `C1S0`, `C1S1`, ..., `C1S5`.

Threading issues aside, the database needs to know which slice to
present. For either processor, the order must be the same, meaning
`C1S0` first.

If `P1` has already processed `C1S0` and `C1S1` and is now working
on `C1S2`, and `P2` has just finished `C1S0` and is requesting a slice,
`C1S1` should be returned (not, say, `C1S3`).

This is already provided for by `esper`: `get_component()` et al yield
a generator. (Not of "slices", though, just individual components.)

Now, say `P1` needed to modify component `c12` in `C1S1`. It could have
done it in two ways.

* Modify `c12` directly in `C1S1`, meaning `P2` would only have this
  version of the world to operate on (when it gets to it). Not
  thread-safe, but probably fast.
* Create a modified copy `c60` of component `c12` in a "holding"
  slice `C1S6`, and replace `c12` in respective entity with `c60`.
  Probably thread-safe if component substitution is atomic, but
  slower due to need for object construction.

`esper` easily allows the latter: call `add_component(...)` and
provide a component of the same `C1` type that an entity already has.
The component will be replaced transparently.

**Question:** would this newly-created component be processed by `P1` in the
same time frame? What about `P2`?

The scheme described above would also require garbage collection for
components that are no longer linked to any entities.

**Question:** is this already done by `esper`?


## Conclusions

Two questions raised in prior section need answers!

Care must be taken to detect dependencies between processors.

If slicing is ever implemented, it must be done at the storage level,
so that a generator doesn't have to construct the slice prior to
yielding it.

Priority queues may also be an unneeded complication ATM. The need for
them will become apparent from FPS drop, if processor running remains
sequential.


## Links

* [Re: Evaluating the Entity Component System model for DDA.][kevinddabucket]
* [Game Programming Patterns / Decoupling Patterns / Component][nystromcomponent]

[kevinddabucket]: http://smf.cataclysmdda.com/index.php?topic=14148.msg295668#msg295668
[nystromcomponent]: http://gameprogrammingpatterns.com/component.html
