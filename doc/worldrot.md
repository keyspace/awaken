# World slicing and rotation

## Intro

Simulating the whole universe seems excessive right now.

"World" can be thought of as a part that's simulated - loaded into
memory and accessible to processors.

"Slicing" refers to the `rogue`-like representation of the world,
where a top-down view is presented, each display character amounting
to certain space. "Certain" as in "specific", having coordinates,
such that distance between two entities could be calculated.

"Rotated" slices can be thought of as representations of the world
on a plane other than Z.


## Tile size

The very first commit to `README.md` set tile size to a somewhat
arbitrary 0.5 meters. A corridor one tile wide allows an average-sized
human to pass through it. And that's that.

The same commit also mentions human-like "characters stretching out
to sleep", and "multi-tile entities". If an average space colonist
was 1.5 meters tall (smaller size -> less food required upon reaching
destination), that would mean three tiles.


## Immediate implementation concerns

* When a human-like character lies down, in which direction does its
  "rest of body" appear? Does the "head" stay on the same tile?

Having a "face direction" is not usual for `rogue`-likes. If it is
to be present at some conditions, it might as well be present always,
just to limit special-casing, and having to explain every decision.

This may also come handy when running (to limit surrealistic strafing),
combat (dodging, crouching, shooting from behind corners), and other
"realism" concerns.

Also, it begs to reduce field of view to 180 degrees, tops.

Mind you - by definition, "absolute realism" is unachievable in
a simulation. The cutoff line will always be arbitrary. It may be
dictated by design limitations, left out as "undesirable", or some other
artificial constraint - but it's always human-made, even if without
explicit reasoning.

* A standing character has one location, that of the tile standing on.
  A character lying down has three locations!

* A panel wall on a Z-plane view has one location, modelled as one
  entity. How many locations on an X-plane view? How many entities?

In other words, are the dimensions of (x,y,z) in meters (0.5,0.5,Z) or
(0.5,0.5,0.5)? The latter seems more uniform - again, less special-
casing. However, it means 2*Z times more entities!

For an example, here's an "escape pod":

```
/z/
╔═╗
║@║
╚═╝

/x/ /y/
╔═╗ ╔═╗
║4║ ║4║
║@║ ║@║
║│║ ║╞║
║║║ ║║║
╚═╝ ╚═╝
```

(0.5,0.5,0.5) could mean much earlier complications due to world
size. Granted, processors don't have to iterate through entities
that "don't do anything". CPU-wise the penalty may still come from
an under-performing implementation of database lookups, or perhaps
caching.

Anyway, this will hit memory first.


## "Default" components

`ecs.md` used a very safe estimate of an "average" entity being 4 KiB in
size. The footprint would probably be much larger for, say, creatures,
but much smaller for do-nothing "prop" entities.

Perhaps this could be reduced further by having a "default body"
component that gets set for all pristine walls, a-la [flyweight
pattern][flyweight].

The above mock-up world could then contain 33 entities for walls alone.
They would each have a separate Location, but the same Body component.
A total of 67 Python objects, as compared to 99 without reuse - about a
third less.

(Without X- and Y-plane views, in a naïve `rogue`-like without floors
and ceilings, that would only be 24. But then again, reasoning about
atmosphere pressure and the like would be much more difficult.)

It seems at first that if the `Collider` processor wanted to "damage"
a wall, it would have to check first if an entity has this kind of
"default" component. However, with `esper`'s implementation this is
not necessary, as demonstrated by the primitive `Mover` processor.
Replacing one component with another can be done transparently, by
adding a component of certain type to an entity that already has one.

So, a "default" `Body` would be replaced by a non-default one for
an entity that's gotten damaged. And a processor like `Mover` above
wouldn't even know the difference!

Eventually, though, the world would "go to noise", with more and more
entities having "custom" components. Solving this can probably be
delayed, though.


## Camera view "depth"

In the mock-up above, Z-plane seems to be at z==3, explaining nicely
why the "ceiling" at zenith is not displayed.

The "depth" of this view before things start to "fade away" will
eventually need to be estabilished.

At first, a magic number of 4 can be chosen, engulfing everything
between the Z-plane where a character's head is (z==3), down (nadir) to
the "floor".


## Conclusions

Having a tile size of (0.5,0.5,0.5) would mean the following.

* Immediately necessitate implementation of multi-tile entities.
* Start reusing components to reduce a world's memory footprint.


## Links

* [Flyweight pattern][flyweight]

[flyweight]: https://en.wikipedia.org/wiki/Flyweight_pattern
