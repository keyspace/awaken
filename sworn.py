# sworn, the window manager

import curses, curses.panel

class Manager(object):
    """Calls .update() on managed object instances.

    Not an ECS processor, but similar in interface.
    """
    def __init__(self):
        self.managed = []
        return

    def add(self, tomanage):
        self.managed.append(tomanage)
        return

    def process(self, dt):
        for m in self.managed:
            m.update()
        return


class CursesWindowManager(Manager):
    """Updates all Curses windows."""
    def __init__(self):
        super().__init__()
        curses.curs_set(False) # don't display cursor
        curses.noecho()
        return

    def process(self, dt):
        # FIXME: insert into proper position when adding
        sorted(self.managed, key = lambda w: w.depth)

        super().process(dt)

        # everything should be panelled, no need to iterate+.update()
        curses.panel.update_panels()
        curses.doupdate()

        return


class CursesInputManager(Manager):
    """Passes user input to current-context Window."""
    def __init__(self):
        super().__init__()
        self.command = curses.ERR # TODO: multi-char
        self.context = None # curses window to getch() from
        return

    def add(self, container):
        """Push current CursesWindow (or derived) to managed list, switch
        context to its window for purposes of input forwarding."""
        super().add(self.context)
        self.context = container
        return

    def process(self, dt):
        """Overriden: handles input on "latest" window only."""

        if self.context is None:
            # TODO: assign some default context
            return

        input = self.context.window.getch()
        # window resize event is passed in as key, so catch here
        if input == curses.KEY_RESIZE:
            self.context.window.erase()
        # otherwise, pass input to currently focused context
        if input != curses.ERR:
            # TODO: multi-key commands
            self.command = input
            self.context.input_handler(self.command)

        return


class Window(object):
    """An abstract window."""
    def __init__(self, width, height):
        # x/y not used ATM? Remove if not needed for resizing.
        self.x = 0
        self.y = 0
        self.width = width
        self.height = height
        self.title = ''
        return

    # TODO: @abstractmethod?
    def update(self):
        raise NotImplementedError


class CursesWindow(Window):
    """A Curses window with title and border in a Curses panel."""
    def __init__(self, width, height):
        super().__init__(width, height)
        self.depth = 0

        self.window = curses.newwin(self.height, self.width, self.y, self.x)
        self.window.nodelay(1) # don't wait for input
        self.window.leaveok(1) # don't move cursor
        self.window.keypad(1)  # interpret escape sequences

        self.panel = curses.panel.new_panel(self.window)

        self.decorate()

        self.input_handler = lambda input: None

        return

    def decorate(self):
        # TODO: prevent overflow
        self.window.box()
        self.window.addstr(0, 1, '╼' + self.title + '╾')
        return

    def update(self):
        # FIXME: do on resizes only?
        self.decorate()
        # DEBUG magicnum: over 9000
        if self.depth > 9000:
            self.panel.top()
        return

    # TODO: add `docked` property that moves the window when needed
    def move(self, x, y):
        """Specify coordinates for top-left corner of panel on screen."""
        self.panel.move(y, x)
        return


class CursesWindowedText(CursesWindow):
    """Plain text in a Curses window.

    When text needs updating, one can set .text property directly.
    Alternatively, set .update_text to a function (e.g. a lambda)
    that is to be called every time .update() is called.
    """
    def __init__(self, width: int = 16, height: int = 3):
        """Width/height currently include borders."""

        super().__init__(width, height)
        self._text = ''
        self.update_text = None
        return

    def update(self):
        if self.update_text is not None:
            self._text = str(self.update_text())
            self.window.addstr(1, 1, ' ' * (self.width - 2))
            self.window.addstr(1, 1, self._text)
        super().update()
        return

    @property
    def text(self):
        return self._text
    @text.setter
    def text(self, t):
        # -2 to account for Curses border
        self._text = str(t)[:self.width-2]
        return


class CursesWindowedPad(CursesWindow):
    """A Curses window with a pad "canvas".
    Done like this since pads by themselves can't be panelled easily.
    """
    def __init__(self, width, height):
        super().__init__(width, height)

        # fill entire Window, minus the CursesWindow borders
        self.pad = curses.newpad(self.height-2, self.width-2)
        #self.pad.nodelay(1) # don't wait for input... not needed for a pad in a window?
        self.pad.scrollok(0)
        return

    def update(self):
        """Overlays pad onto window, then updates window."""
        # destination window and source pad sizes
        wy, wx = self.window.getmaxyx()
        py, px = self.pad.getmaxyx()
        # -2 to account for drawn window border
        x = min(wx-2, px)
        y = min(wy-2, py)
        self.pad.overlay(self.window, 0,0, 1,1, y,x)

        super().update()
        return

    # FIXME: Misplaced! Turn into a lambda to be set as self.pad_update?
    def fill_pad_from_cache(self, cache):
        """Translation layer (aka glue code) from game LevelCache to
        curses' pad and (momentarily) window."""
        # TODO: resize in separate function?
        # FIXME: +1 height: writing wide character scrolls anyway?
        self.pad.resize(cache.height+1, cache.width)
        for y in range(cache.height):
            for x in range(cache.width):
                self.pad.addch(y, x, cache.get_contents(x,y))
        return

    # Used once for debugging.
    # TODO: could use containing window's width/height...
    @property
    def padwidth(self):
        py, px = self.pad.getmaxyx()
        return px
    @property
    def padheight(self):
        py, px = self.pad.getmaxyx()
        return py
