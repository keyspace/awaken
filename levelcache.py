# displaying entities that have both a body and are visible

import random

import esper

import physics
import vision


class LevelCache(object):
    """Component: game "level" (for lack of a better term).

    Represents a slice of the "world" in top-down view.
    Used as a quick way to fill Curses' camera pad.

    Not well-suited (yet) for location-based lookups."""
    def __init__(self, width = 0, height = 0):
        self.width = width
        self.height = height
        self.level = 0 # TODO: use

        # list of lists, rows of columns (TODO: reverse?)
        self.default = ' '
        self._contents = [[self.default]*width for _ in range(height)]

        return

    # FIXME: like this due to the way `contents` is filled
    def get_contents(self, x, y):
        try:
            return self._contents[y][x]
        except IndexError:
            return self.default

    # FIXME: ugh...
    def set_contents(self, x, y, glyph):
        # TODO: can remove check when walking off-map terminates simulation
        if x < 0 or y < 0:
            return
        try:
            self._contents[y][x] = glyph
        except IndexError:
            pass
        return


class LevelTracker(esper.Processor):
    """Updates the contents of `LevelCache`s from world."""
    def __init__(self):
        super().__init__()
        return

    # FIXME: massively under-performing
    def process(self):
        """TODO"""
        for ent, cache in self.world.get_component(LevelCache):
            # first draw _all_ floors...
            for ent, (pos, _, glyph) in self.world.get_components(
                    physics.Position, physics.Surface, vision.Glyph):
                cache.set_contents(pos.x, pos.y, glyph.glyph)
            # then overwrite with bodies :/
            for ent, (pos, _, glyph) in self.world.get_components(
                    physics.Position, physics.Body, vision.Glyph):
                cache.set_contents(pos.x, pos.y, glyph.glyph)
        return


# DEBUG
def randomise(level, world):
    """Debug module-level function to randomise a LevelCache.

    Creates "terrain" entities (i.e. Surface without Body) in World
    and immediately adds them to LevelCache.

    Cache must have size set.
    """
    if level.width == 0 or level.height == 0:
        return

    # one of these symbols
    glyphs = ['.']*15 + [',']*5 + ['o*▲▼']
    glyph = lambda: random.choice(''.join(glyphs))

    # "construct" new contents...
    contents = []
    for y in range(level.height):
        row = []
        for x in range(level.width):
            g = glyph()
            row.append(g)

            tile = world.create_entity()
            world.add_component(tile, physics.Surface())
            world.add_component(tile, physics.Position(x = x, y = y))
            world.add_component(tile, vision.Glyph(g))
        contents.append(row)
    # ... and substitute them for the current ones
    level._contents = contents

    return
