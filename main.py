#!/usr/bin/env python3
# nothing to see here, move along...

import curses
import time

import esper

import sworn

import debug
import levelcache
import physics
import vision


# TODO: read from config, assign to "actions"
def control(world, creature, input):
    """Passed as an input handler to CAMERA window.

    The latter is processed by CursesInputManager, a non-esper "processor".

    Current state of affairs is: this adds a Velocity component, Mover
    processor unconditionally removes it on the next world "tick". In
    effect, most "ticks" don't have to deal with Velocity at all.
    """

    dx = 0
    dy = 0
    if input in [curses.KEY_HOME,
                 curses.KEY_LEFT,
                 curses.KEY_END,
                 *[ord(k) for k in '741yhb']]:
        dx -= 1
    if input in [curses.KEY_PPAGE,
                 curses.KEY_RIGHT,
                 curses.KEY_NPAGE,
                 *[ord(k) for k in '963uln']]:
        dx += 1
    if input in [curses.KEY_HOME,
                 curses.KEY_UP,
                 curses.KEY_PPAGE,
                 *[ord(k) for k in '789yuk']]:
        dy -= 1
    if input in [curses.KEY_END,
                 curses.KEY_DOWN,
                 curses.KEY_NPAGE,
                 *[ord(k) for k in '123jbn']]:
        dy += 1

    if dx != 0 or dy != 0:
        world.add_component(creature, physics.Velocity(dx, dy))

    return


def main(stdscr):
    ##
    ## esper init
    ##
    world = esper.World()

    # esper 0.9.7 does not allow adding components to entity immediately
    creature = world.create_entity()
    world.add_component(creature, physics.Body())
    world.add_component(creature, physics.Position(x = 2, y = 2))
    world.add_component(creature, vision.Glyph('@'))

    level = world.create_entity()
    cache = levelcache.LevelCache(width = 20, height = 20)
    world.add_component(level, cache)
    # for the same reason, can't pass "template to fill with" here
    levelcache.randomise(cache, world)

    # processors
    world.add_processor(physics.Mover())
    world.add_processor(levelcache.LevelTracker())

    ##
    ## sworn init
    ##
    camwin = sworn.CursesWindowedPad(curses.COLS, curses.LINES)
    camwin.title = 'CAMERA'
    camwin.input_handler = lambda input: control(world, creature, input)

    wm = sworn.CursesWindowManager()
    wm.add(camwin)

    im = sworn.CursesInputManager()
    im.add(camwin) # give input to this window

    ##
    ## generic init
    ##
    maxticks = 1000 # once a millisecond

    now = time.time()
    elapsed = 0.0
    prevtick = now
    tps = 0.0

    ##
    ## DEBUG init
    ##
    wm.add(debug.gen('t/s',
                     lambda: round(tps, 3)))
    wm.add(debug.gen('KEYCODE',
                     lambda: im.command))
    wm.add(debug.gen('ENTITIES',
                     lambda: len(world._entities)))
    wm.add(debug.gen('COMPONENTS',
                     lambda: sum(
                         [len(world._components[type_])
                          for type_ in world._components])))
    wm.add(debug.gen('x/y/z',
                     lambda: str(world.component_for_entity(
                         creature, physics.Position))))

    try:
        while True:
            world.process()

            camwin.fill_pad_from_cache(cache)

            wm.process(elapsed)
            im.process(elapsed)

            now = time.time()
            elapsed = now - prevtick
            prevtick = now
            tps = 1. / elapsed

            time.sleep(1. / maxticks)
    except KeyboardInterrupt:
        return


if __name__ == '__main__':
    curses.wrapper(main)
