# loading levels from JSON

import json
import jsonschema
import os


def load(filename, world):
    """Loads the contents of a JSON file into esper's World."""
    with open(filename, 'r') as fd:
        entities = json.load(fd, object_hook = parse_object)
    #TODO
    return

# FIXME: schema file validated every time, instead of once
def parse_object(obj):
    """Validates passed JSON object, and returns TODO.

    """
    # determine the schema to validate with, bounce raw if not set
    if 'schema' not in obj:
        return obj
    schema = obj['schema']

    # validate object
    fname = os.path.join('data', schema + '.schema.json')
    try:
        with open(fname, 'r') as fd:
            jsonschema.validate(obj, json.load(fd))
    except jsonschema.exceptions.ValidationError as e:
        # TODO: raise exception with malformed object?
        raise

    # at this point, the object should only be describing one entity
    # TODO
    # ent = entity_from_object(obj, schema)

    return


load('data/boxworld.json', [])
