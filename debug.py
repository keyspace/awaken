# debugging - not strictly necessary for "normal play"

import curses

import sworn


def _gen():
    """Debug window generator implementation.

    Currently yields a window of fixed size.
    """
    y = 1
    win = None

    while True:
        (title, fun) = (yield win)

        win = sworn.CursesWindowedText()
        win.title = title
        win.update_text = fun
        win.depth = 9001 # magicnum: over 9000
        # FIXME: _curses_panel.error if window too small
        # TODO: just set `docked`, let sworn move
        win.move(curses.COLS - win.width - 1, y)
        y += win.height
    return

def gen(title, fun):
    """Yields a new debug text window and "docks" it to the right.

    The window will call the provided function every time it's
    updated.

    This function is a convenience interface to wrap arguments into
    a tuple.
    """
    return wingen.send((title, fun))


# prime module-level generator
# NOTE: module loading side-effect OK for debugging?..
wingen = _gen()
wingen.send(None)
