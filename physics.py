# poor man's physics

import esper


class Velocity(object):
    """Component: poorly-vectorised velocity, essentially speed.

    Currently arbitrary for both units of distance and time."""
    def __init__(self, x = 0, y = 0, z = 0):
        """TODO"""
        self.x = x
        self.y = y
        self.z = z
        return


class Position(object):
    """Component: orthogonal coordinates."""
    def __init__(self, x = 0, y = 0, z = 0):
        """TODO"""
        self.x = x
        self.y = y
        self.z = z
        return

    # DEBUG
    def __str__(self):
        return str([self.x, self.y, self.z])


class Body(object):
    """Component: body of a one-tile entity."""
    def __init__(self):
        return


class Surface(object):
    """Component: non-body "terrain", e.g. floor."""
    def __init__(self):
        return


class Mover(esper.Processor):
    """Poor man's Physics. Only pushes things around."""
    def __init__(self):
        super().__init__()

    def process(self):
        for ent, (vel, pos) in self.world.get_components(Velocity, Position):
            pos.x += vel.x
            pos.y += vel.y
            # FIXME: band-aid "friction" to prevent continuous movement
            self.world.remove_component(ent, Velocity)
        return
