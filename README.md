# Awaken

You wake up from cryogenic sleep on a spaceship sent to colonise
humanity's first other solar system.

Something has definitely gone wrong. What could it be?..

A plague wiped out the maintenance generation. You're a reserve
cybernetic organism meant to bootstrap a new one. You have been summoned
by the Observer process. You may return to your casket once the task is
complete.

The vessel has been boarded by aliens. You survived by chance when the
emergency unthawing of your cell kicked in. Lucky for you, you're a
military biologist, so it's likely you can find a way to kill those
"rats"... If, of course, there is one. But all you really want is to get
back to blissful slumber.

There has been a malfunction in the monitoring system. Life support is
failing, and the Observer needs a reboot. You are an illiterate peasant,
and you've been "chosen" as one of the expendable meatbags to guard
engineers while they attempt to do so. Eventually, you discover this was
a suicide run all along. How unfortunate then for everyone who thought
otherwise that you're the sole survivor.

An asteroid collision appears statistically inevitable. Your casket
bank has been randomly selected for "sacrificial staging". The entire
bank has been revived due to this emergency. There is nowhere to
relocate except where the maintenance generation always resides - at the
ship's core, but they don't seem welcoming. In fact, they've just cut
communications and diverted oxygen.

The vessel has been taken over by a rebel faction of luddites. You have
been "brought back from the dead" to "atone for your sins in flesh".
You'll be working on the farms until you die...


## Status

> Nothing to see here, move along!

Initial development sandbox with no structure and no rules.

Refining "window manager", introducing "entity component processor system".


## Design

Easy to explain:

* "levels"
* world size, number of creatures, and other world options
* succession games instead of multiplayer
* story arc or sandbox
* mapgen described in data _or_ algorithms
* implants, mutations, crammed rooms, short ranged distances, smoke
  not dissipating, and other shenanigans

Finally, run your imagination naked with technology replacing magic!


## Roadmap

1. Skeleton - basic interface.
2. Nothing-to-do sandbox and simple tutorial go-fetch mission - expanded
   interface.
3. Story arc: "bootstrap a maintenance generation" - complex interface,
   basic crafting. A cyborg that doesn't need almost anything in terms of
   basic necessities has to do several independent go-fetch missions, and
   place the retrieved "artifacts" in the cloning machine.
4. Story arc: "invasion" - basic needs, basic combat, expanded crafting.
   Several nested go-fetch missions to retrieve an alien sample and
   manufacture a targeted anti-measure.
5. Expanded story arc: "invasion" - basic factions, expanded needs,
   expanded combat. Several independent go-figure missions to unthaw
   characters, then persuade missions for them to join faction. Sporadic
   go-fetch to gather equipment and overpower in combat.


## Notes

* Shall not consider compatibility with proprietary operating systems.
  Enough is enough!
* Curses will get you anywhere you want. Eschew graphics - there are more
  worthy aspects that can benefit from parallelisation.
* Creatures, terrain, furniture, objects to be multi-tiled. No more tiny
  mainframes, stairs, doors, or sofas!
* Do SI units from the get-go. Yes, interfaces everyhere.
* A tile is 0.5 meters. Yes, characters stretch out to sleep in gravity.
* The player character can do everything a non-player one can, and
  vice-versa. They just use a different UI.
* Shooting too much might result in rapid decompression.
* Missions are puzzles. If there's only one way to finish them, then
  they're only fun once.


## License

Everything in this repository is licensed under GPLv3. See `LICENSE.txt`.
