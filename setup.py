#!/usr/bin/env python3

from distutils.core import setup

#exec(open('./src/metadata.py').read())
__version__ = 0.0.1
__url__ = 'https://gitlab.com/keyspace/awaken'

setup(name='awaken',
      version=__version__,
      description='Roguelike on a spaceship.',
      author='keyspace',
      author_email='keyspace@veox.pw',
      url=__url__,
      packages=['awaken'],
      classifiers=[
          'Programming Language :: Python :: 3',
      ],
      install_requires=[
          'esper==0.9.7',
          'jsonschema'
      ]
)
